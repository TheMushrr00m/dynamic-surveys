var mongoose = require("mongoose"),
  faker = require("faker"),
  Poll = require("./models/Poll"),
  poll;

poll = new Poll({
  question: faker.lorem.sentence,
  answers: [
    faker.lorem.words
  ],
  user: faker.lorem.words
});

poll.save(function(err, poll) {
  if(err) return console.log(err);
  console.log("Saved!");
});
