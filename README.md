# Dynamic Surveys - Node Version

***
## Created in Cancún, México :sunglasses:

### Develop by ###
* __Gabriel Cueto__
+ __Juan Pablo__
- __Gloobe Team__

### Instructions ###
* Clone the repo.
- cd into the folder.
+ npm install => To install dependencies.
- (pm2 | forever) app.js => To start the app.

