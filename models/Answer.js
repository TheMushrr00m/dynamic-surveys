// Modelo para las opciones de respuesta a las preguntas.
var mongoose = require("mongoose"),
  Vote = require("./Vote.js"),
  Schema = mongoose.Schema;
  answerSchema = new Schema({
    text: { type: String },
    votes: [Vote]
  });

module.exports = mongoose.model("Answer", answerSchema);
