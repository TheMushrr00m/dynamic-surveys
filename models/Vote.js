// Modelo para las respuestas de los usuarios a las encuestas..
var mongoose = require("mongoose"),
  voteSchema = mongoose.Schema({
    ip: { type: String },
  });

module.exports = mongoose.model("Vote", voteSchema);
