// Modelo Pregunta
var mongoose = require("mongoose"),
  Poll = require("./Poll"),
  Answer = require("./Answer");

var questionSchema = mongoose.Schema({
  title_question: { type: String, required: true },
  Answers:        [Answer]
});

module.exports = mongoose.model("Question", questionSchema);
