"use strict";
var mongoose = require("mongoose"),
  User = require("./User"),
  Question = require("./Question");

var pollSchema = mongoose.Schema({
  title_poll: { type: String, required: true },
  questions: [Question],
});

module.exports = mongoose.model("Poll", pollSchema);
