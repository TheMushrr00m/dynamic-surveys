"use strict";
const mongoose = require("mongoose");
const Poll = require("./Poll");

let userSchema = mongoose.Schema({
  fname: { type: String, required: true },
  lname:  { type: String, required: true },
  email:      { type: String, required: true },
  password:   { type: String, required: true },
  type:       { type: String, default: "users" },
  user_id:    { type: Number },
  polls:      [Poll]
});

module.exports = mongoose.model("User", userSchema);
