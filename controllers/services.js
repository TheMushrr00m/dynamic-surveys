var jwt = require('jwt-simple'),
  moment = require('moment'),
  config = require('./config/config');

exports.createToken = function(user) {
  var payload = {
    sub: user._id,
    iat: moment().unix(),
    exp: moment().add(15, "days").unix(),
  };
  return jwt.encode(payload, config.TOKEN_SECRET);
};
