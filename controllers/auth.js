var mongoose = require("mongoose"),
  bodyParser = require("body-parser"),
  User = mongoose.model("User"),
  service = require("./controllers/service");

exports.emailSignup = function(req, resp) {
  var user = new User({
    First_Name: req.body.fname,
    Last_Name: req.body.lname,
    Email: req.body.email,
    PAssword: req.body.pass
  });

  user.save(function(err) {
    if(!err) return resp.status(200).send({ token: service.createToken(user) });
  });
};

exports.emailLogin = function(req, resp) {
  User.findOne({
    Email: req.body.email.toLowerCase(),
    Password: req.body.pass
  }), function(err, user) {
    if(!err) return resp.status(200).send({ token: service.createToken(user) });
  }
};
