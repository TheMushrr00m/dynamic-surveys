var mongoose = require("mongoose"),
  Poll = require("../../../models/Poll");

exports.findAllPolls = function(req, resp) {
  console.log(req.params);
  Poll.find({})
  .where("id").equals(req.params.user_id)
  .exec(function(err, polls) {
    if(err) return resp.status(500).send(err.message);
    console.log("GET /api/v1/users/:user_id/polls");
    resp.status(200).jsonp(polls);
  });
};

exports.findById = function(req, resp) {
  Poll.findById(req.params._id, function(err, poll) {
    if(err) return resp.send(500, err.message);
    console.log("GET /api/v1/users/:user_id/polls/" + req.params._id);
    resp.status(200).jsonp(poll);
  });
};

exports.addPoll = function(req, resp) {
  console.log("POST");
  console.log(req.body);

  var poll = new Poll({
    question: req.body.question,
    answers: req.body.answers,
    user: req.params.user_id,
  });

  poll.save(function(err, poll) {
    if(err) return resp.status(500).send(err.message);
    resp.status(200).jsonp(tvshow);
  });
};

exports.updatePoll = function(req, resp) {
  Poll.findById(req.params.poll_id, function(err, poll) {
    poll.question = req.body.question,
    poll.answers = req.body.answers,

    poll.save(function(err) {
      if(err) return resp.status(500).send(err.message);
      resp.status(200).jsonp(poll);
    });
  });
};

exports.deletePoll = function(req, resp) {
  Poll.findById(req.params.poll_id, function(err, poll) {
    poll.remove(function(err) {
      if(err) return resp.status(500).send(err.message);
      resp.status(200).send();
    });
  });
};
