'use strict';
const User = require("../../../models/User");
const mongodb = require("mongodb");
const mquery = require("mquery");
const config = require("../../../config/config");


// Método para crear usuarios
exports.addUser = (req, resp) => {
  let user_id, user;
  // Verifica que en la petición vengan los parametros necesarios para crear al usuario.
  if(req.body.hasOwnProperty("fname") && req.body.hasOwnProperty("lname") &&
      req.body.hasOwnProperty("email") && req.body.hasOwnProperty("password")) {
    // Obtiene el último registro en la colección para definir el siguiente "user_id".
    mongodb.connect(config.DB_CONNECTION, (err, db) => {
      if(err) handleError(err);
      mquery(db.collection("users")).findOne({})
      .sort({user_id: -1}) // El valor -1 establece que ordene de forma descendiente.
      .exec((err, userResult) => {
        if (err) handleError(err);
        else if(userResult === undefined || userResult === null) user_id = 1;
        else user_id = userResult.user_id + 1;
        // Define la estructura del nuevo usuario a crear.
        user = new User({
          first_name: req.body.fname,
          last_name: req.body.lname,
          email: req.body.email,
          password: req.body.password,
          user_id: user_id
        });
        // Registra el nuevo usuario en la BD.
        user.save((err, user) => {
          if(err) handleError(err);
          return resp.status(200).jsonp({ data: [user], errors: [] });
        });
      });
    });
  }
  else {
    return resp.status(500).jsonp({ data: [], errors: [{ status: 500, title: "Datos incompletos." }]});
  }
};


// Método para obtener todos los usuarios
exports.findAllUsers = (req, resp) => {
  User.find({}).exec((err, users) => {
    if(err) return resp.status(500).jsonp({ data: [], errors: [err] });
    else if(users === undefined || users === null || users.length === 0)
      return resp.status(500).jsonp({ data: users, errors: [{ status: 500, title: "No hay registros en la BD." }]});
    return resp.status(200).jsonp({ data: users, errors: [] });
  });
};


// Método para obtener un usuario en especifico.
exports.findById = (req, resp) => {
  User.findOne({ "user_id": req.params.user_id }, (err, user) => {
    if(err) return resp.status(500).jsonp({ data: [], errors: [err] });
    else if(user === undefined || user === null)
      return resp.status(500).jsonp({ data: [], errors: [{ status: 500, title: "Usuario no encontrado" }]});
    return resp.status(200).jsonp({ data: [user], errors: [] });
  });
};


// Método para actualiza la información de un usuario.
exports.updateUser = (req, resp) => {
  let update = {};
  let options = { new: true };
  // Establecemos las propiedades a modificar con las recibidas en la petición.
  for(let property in req.body) {
    update[property] = req.body[property];
  }
  User.findOneAndUpdate({ "user_id": req.params.user_id }, update, options, (err, user) => {
    if(err) return resp.status(500).jsonp({ data: [], errors: [err] });
    else if(user === undefined || user === null)
      return resp.status(200).jsonp({ data: [], errors: [{ status: 500, title: "Usuario no encontrado" }]});
    resp.status(200).jsonp({ data: [user], errors: [] });
  });
};


// Método para eliminar usuarios.
exports.deleteUser = (req, resp) => {
  User.findOneAndRemove({ "user_id": req.params.user_id }, (err, user) => {
    if(err) return resp.status(500).jsonp({ data:[], errors: [err] });
    resp.status(200).jsonp({ data: [user], errors: [] });
  });
};
