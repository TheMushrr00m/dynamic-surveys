'use strict';
const Polls = require("../controllers/api/v1/polls");
const Users = require("../controllers/api/v1/users");
const middleware = require('../controllers/middleware');

// ===============================
// Rutas del API
// ===============================
// Users
exports.addUser = Users.addUser;
exports.getUsers = Users.findAllUsers;
exports.getUser = Users.findById;
exports.updateUser = Users.updateUser;
exports.deleteUser = Users.deleteUser;

// Polls
exports.listUserPolls = Polls.findAllPolls;

// ===============================
// Rutas generales
// ===============================
// '/'
exports.index = (req, resp) => {
  resp.send("<h1>Dynamic Surverys</h1>");
};

// Cualquier ruta no definida utilizará está función
exports.error = (req, resp) => {
  resp.redirect(301, '/');
};
