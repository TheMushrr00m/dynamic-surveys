'use strict';
const express = require("express");
const app = express();
const session = require("express-session");
const methodOverride = require("method-override");
const logger = require("express-logger");
const bodyParser = require('body-parser');
const routes = require("./config/routes");
const path = require("path");
const cors = require("cors");
const mongoose = require("mongoose");
const middleware = require("./controllers/middleware");
const nunjucks = require("nunjucks");
const config = require("./config/config");

// ==============================
// Configuraciones generales
// ==============================
app.set("port", config.PORT);
app.set("views", path.join(__dirname, "views"));
//app.use(express.favicon());
app.use(express.static(path.join(__dirname, "public")));
// Permite "parsear" las peticiones realizadas por el método POST.
app.use(bodyParser.urlencoded({
  extended: false
}));
// Permite "parsear" las peticiones con formato JSON.
app.use(bodyParser.json());
app.use(methodOverride());
app.use(logger({ path: "./log.txt" }));
app.use(cors());
app.set("views", path.join(__dirname, "views"));
// Establecemos una 'secret_key' para mantener las sesiones.
app.use(session({
  // Encoded: Base64
  secret: "R2xvb2JlIFBvbGxz="
}));
// Manejando errores de forma elegante.
app.use((err, req, resp, next) => {
  if(!err) return next();
  console.log(err.stack);
  resp.status(500).jsonp({ data: [], errors: [err] });
});

//==========================================
// Rutas del API.
//==========================================
// Usuarios
app.route("/api/v1/users")
    .get(routes.getUsers)
    .post(routes.addUser);
app.route("/api/v1/users/:user_id")
    .get(routes.getUser)
    .put(routes.updateUser)
    .patch(routes.updateUser)
    .delete(routes.deleteUser);
app.post("/auth/login"); // Ruta de login.
app.get("/api/v1/users/:user_id/polls", routes.listUserPolls); // Obtiene las encuestas disponibles del usuario.
app.get("/api/v1/users/:_id/polls/:_id"); // Obtiene la encuesta especificada por el usuario.
app.post("/api/v1/users/:_id/polls"); // Crea una nueva encuesta.
app.post("/api/v1/vote"); // Registra las respuestas de las encuestas.

// Rutas generales.
app.get("/", routes.index);
app.get("/dashboard", middleware.ensureAuthenticated, (req, resp) => {
});
app.all("*", routes.error);

// Iniciamos el servidor.y la DB.
mongoose.connect("mongodb://localhost/polls", (err) => {
  if(err) return console.log("ERROR: connecting to Database." + err);
  app.listen(app.get("port"), () => {
    return console.log("Server running on port %d!", app.get("port"));
  });
});
